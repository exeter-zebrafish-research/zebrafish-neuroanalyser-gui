"""Main gui functions"""
import logging
# import time
from rich.logging import RichHandler
from zebrafish_neuroanalyser_gui import settings
from zebrafish_neuroanalyser_gui.align import run as run_gui_align # noqa:F401
from zebrafish_neuroanalyser_gui.qt5 import run as run_gui_legacy # noqa:F401
from zebrafish_neuroanalyser_gui.tk_app import (
        run as run_gui_classical, # noqa:F401
        get_existing_directory, # noqa:F401
)

def initialise_logger(logger_obj, log_file=None):
    """
    Initialises the root logger
    """
    if logger_obj.handlers:
        return
    handler = RichHandler(rich_tracebacks=True)
    logger_obj.addHandler(handler)
    handler.setLevel(logging.INFO)
    logger.setLevel(logging.INFO)
    # Add a file logger
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    if log_file is None:
        log_file = settings.get_settings().log_file

    file_handler = logging.FileHandler(log_file)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger_obj.addHandler(file_handler)


logger = logging.getLogger(__package__)
initialise_logger(logger)
