# -*- coding: utf-8 -*-
"""Handle getting of settings

J. Metz <metz.jp@gmail.com>
"""
from pathlib import Path
from dataclasses import dataclass
import platformdirs
import tomli
import tomli_w

__default_state_path__ = platformdirs.user_state_path('zebrafish_neuroanalyser_gui')
if not __default_state_path__.is_dir():
    __default_state_path__.mkdir(parents=True)

@dataclass
class Settings:
    log_file: str


def get_settings() -> Settings:
    """
    Get settings from standard location
    """
    folder = platformdirs.user_state_path('zebrafish_neuroanalyser_gui', ensure_exists=True)
    path = folder / "settings.toml"
    settings = _create_default_settings()
    if path.is_file():
        settings.update(tomli.load(path.open('rb')))
    return Settings(**settings)


def _create_default_settings_file(path: Path) -> dict:
    settings = _create_default_settings()
    with path.open("wb") as file_out:
        tomli_w.dump(settings, file_out)
    return settings


def _create_default_settings() -> dict:
    return {
            "log_file": (__default_state_path__ / "log.log").as_posix()
    }

