# -*- coding: utf-8 -*-
"""Tk-based main running app

J. Metz <metz.jp@gmail.com>
"""
import math
import string
import logging
import colorsys
from pathlib import Path
import tkinter as tk
from  tkinter import ttk
from tkinter import simpledialog
from PIL import Image, ImageTk  # type: ignore
import numpy as np
import scipy.ndimage as ndi  # type: ignore
# s = ttk.Style()
# print(s.theme_names())
# s.theme_use('')
# print(s.theme_use(''))
import zebrafish_neuroanalyser.io as zio
from zebrafish_neuroanalyser import utility
from zebrafish_neuroanalyser_gui.tk_app import get_existing_filenames
if not hasattr(Image, "Resampling"):
    # Handle older PIL versions
    Image.Resampling = type("Resampling", (object,), {"NEAREST": Image.NEAREST})


logger = logging.getLogger(__name__)
__submodule_folder__ = Path(__file__).parent
__root_folder__ = __submodule_folder__.parent.parent


def hsv_to_rgb_hex(hue, saturation, value):
    rgb_vec = colorsys.hsv_to_rgb(hue, saturation, value)
    return '#' + ''.join(f'{int(255 * num):02X}' for num in rgb_vec)


def run(files=(), channel=None):
    """
    Runs the application
    """
    app = Application(filenames=files, channel=channel)
    app.run()


class Application:  # pylint: disable=too-few-public-methods
    """
    Root TK application, creates root object and AlignWindow
    """
    # submodule_folder = Path(__file__).absolute().parent

    def __init__(self, filenames=None, channel=None):
        width = 800
        height = 800
        data = np.random.randint(0, 255, (10, 800, 800), 'uint8')
        if data.ndim == 2:
            data = [data]
        for zind in range(1, len(data)):
            data[zind] = ndi.gaussian_filter(data[zind], zind)

        self._root = tk.Tk()
        self.style = ttk.Style(self._root)
        self._root.title('Zebrafish-Neuroanalyser Aligner')
        # root.tk.call('source', 'breeze-dark.tcl')
        self._root.grid()
        # ws['bg'] = '#AC99F2'
        self.window = AlignWindow(
            self._root,
            width,
            height,
            data=data,
            files=filenames,
            channel=channel)
        self._root.reference_image_window = ReferenceWindow(
                self._root,
                title="Reference Image")

        self._root.grid_columnconfigure(0,weight=1)
        self._root.grid_rowconfigure(0,weight=1)
        self._root.resizable(True, True)
        # create the sizegrip
        size_grip = ttk.Sizegrip(self._root)
        size_grip.grid(row=1, sticky=tk.SE)

    def run(self):
        """Run the main loop"""
        self._root.mainloop()


class AutoScrollbar(ttk.Scrollbar):  # pylint: disable=too-many-ancestors
    """
    A scrollbar that hides itself if it's not needed.
    Works only for grid geometry manager
    """
    def set(self, first, last):
        if float(first) <= 0.0 and float(last) >= 1.0:
            self.grid_remove()
        else:
            self.grid()
            ttk.Scrollbar.set(self, first, last)

    def pack(self, **kw):
        """Unimplemented pack"""
        raise tk.TclError('Cannot use pack with the widget ' + self.__class__.__name__)

    def place(self, **kw):
        """Unimplemented place"""
        raise tk.TclError('Cannot use place with the widget ' + self.__class__.__name__)


def load_reference_image(root_folder=__root_folder__):
    """Loads reference image"""
    if utility.IS_ZIP:
        reference_image_file = utility.ARCHIVE.open("zebrafish_neuroanalyser_gui/reference_brain_max.jpg")
        image_data = reference_image_file.read()
        image = ImageTk.PhotoImage(data=image_data)
    else:
        reference_image_file = root_folder / "zebrafish_neuroanalyser_gui" / "reference_brain_max.jpg"
        image = ImageTk.PhotoImage(file=reference_image_file)
    return image


def load_reference_points(root_folder=__root_folder__):
    if utility.IS_ZIP:
        rois_file_obj = utility.ARCHIVE.open("zebrafish_neuroanalyser/labels/rois.txt")
    else:
        rois_file = root_folder / "zebrafish_neuroanalyser" / "labels" / "rois.txt"
        rois_file_obj = rois_file.open(mode='rb')
    pts = []
    letters = []
    for line in rois_file_obj:
        parts = line.split(b",")
        pts.append([float(s) for s in parts[1:]])
        letters.append(parts[0])
    rois_file_obj.close()
    # pts = np.array(pts)
    # pts = pts[:,[2,1,0]]
    return pts, letters

class ReferenceWindow(tk.Toplevel):
    def __init__(self, *args, title=None, root_folder=__root_folder__, **kwargs):
        super().__init__(*args, **kwargs)
        if title is None:
            if len(args) > 0:
                title= args[0].title
            else:
                title = "ReferenceWindow"
        self.title(title)
        self.border = 20
        self.image = load_reference_image()
        self.canvas = tk.Canvas(
                self,
                width = self.image.width() + 2 * self.border,
                height = self.image.height() + 2 * self.border)
        self.canvas.grid(row=0, column=0, sticky='nswe')
        self.canvas.update()  # wait till canvas is created
        self.canvas.create_image(self.border, self.border, anchor=tk.NW, image=self.image)

        pts, letters = load_reference_points()
        npts = len(pts)
        self.colors = tuple(
            hsv_to_rgb_hex(i / npts, 1, 1) for i in range(npts))
        radius = 5
        scalefact = 480.0 / 1406.0

        for coord, lett, color in zip(pts, letters, self.colors):
            pt_x = scalefact * coord[1] + self.border
            pt_y = scalefact * coord[0] + self.border
            self.canvas.create_text(pt_x, pt_y + 2 * radius + 2, text=lett, fill=color)
            self.canvas.create_oval(
                pt_x - radius, pt_y - radius,
                pt_x + radius, pt_y + radius,
                width=1,
                # fill=color_hex,
                outline=color,
                stipple="gray25",
                # tags=("point",),
            )


class CanvasImage:  # pylint: disable=too-many-instance-attributes
    """ Display and zoom image """
    def __init__(self, parent, data, app=None, **create_image_kwargs):
        """ Initialize the ImageFrame """
        self.radius_circle = 5
        self.imscale = 1.0  # scale for the canvas image zoom, public for outer classes
        self.__delta = 1.3  # zoom magnitude
        # self.__filter = Image.ANTIALIAS
        # could be: NEAREST, BILINEAR, BICUBIC, ANTIALIAS
        self.__filter = Image.Resampling.NEAREST
        self.__previous_state = 0  # previous state of the keyboard
        self.selected_points = None
        self.points = {}
        self.point_ids = string.ascii_lowercase
        self.data = data  # data for image, should be public for outer classes
        # Create ImageFrame in parent widget
        self._parent = parent
        self.__frame = ttk.Frame(parent)
        self.__curr_img = None
        self.__scale = None
        self._app = app
        # Vertical and horizontal scrollbars for canvas
        hbar = AutoScrollbar(self.__frame, orient='horizontal')
        vbar = AutoScrollbar(self.__frame, orient='vertical')
        hbar.grid(row=1, column=0, sticky='we')
        vbar.grid(row=0, column=1, sticky='ns')
        # Create canvas and bind it with scrollbars. Public for outer classes
        self.canvas = tk.Canvas(self.__frame, highlightthickness=0,
                                xscrollcommand=hbar.set, yscrollcommand=vbar.set)
        self.create_image_kwargs = create_image_kwargs
        self.canvas.grid(row=0, column=0, sticky='nswe')
        self.canvas.update()  # wait till canvas is created
        hbar.configure(command=self.__scroll_x)  # bind scrollbars to the canvas
        vbar.configure(command=self.__scroll_y)
        # Bind events to the Canvas
        self.canvas.bind('<Configure>', lambda event: self.__show_image())  # canvas is resized
        # self.canvas.bind("<ButtonPress-1>", self.move_point_from)
        self.canvas.bind("<B1-Motion>", self.move_point_to)
        self.canvas.bind("<ButtonRelease-1>", self.add_point)
        self.canvas.bind('<ButtonPress-3>', self.__move_from)  # remember canvas position
        self.canvas.bind('<B3-Motion>',     self.__move_to)  # move canvas to the new position
        self.canvas.bind('<MouseWheel>', self.__wheel)  # zoom for Windows and MacOS, but not Linux
        self.canvas.bind('<Button-5>',   self.__wheel)  # zoom for Linux, wheel scroll down
        self.canvas.bind('<Button-4>',   self.__wheel)  # zoom for Linux, wheel scroll up
        # Handle keystrokes in idle mode, because program slows down on a weak computers,
        # when too many key stroke events in the same time
        self.canvas.bind(
                '<Key>',
                lambda event: self.canvas.after_idle(self._keypress, event))
        self.canvas.bind('<Motion>', self._motion)  # handle mouse motion

        self.load_image()


    def _motion(self, _):
        self.deselect()
        self.select()

    def deselect(self):
        """Deselect any selected points"""
        if self.selected_points is None:
            return
        if "current" in self.canvas.gettags(self.selected_points):
            return
        self.canvas.itemconfigure(self.selected_points, outline="green")
        for idd, color in zip(self.selected_points, self.selected_colors):
            self.canvas.itemconfigure(idd, outline = color)
        self.selected_points = None

    def select(self):
        """Select point"""
        obj_ids = self.canvas.find_withtag('current')
        tags = self.canvas.gettags(obj_ids)
        if "point" in tags:
            logger.debug("Point selected: %s", obj_ids)
            self.selected_colors = tuple(self.canvas.itemcget(idd, "fill") for idd in obj_ids)
            self.canvas.itemconfigure(obj_ids, outline="yellow")
            self.selected_points = obj_ids

    def get_canvas_xy(self, event):
        """Convenience to get canvas x and y"""
        pt_x = self.canvas.canvasx(event.x)
        pt_y = self.canvas.canvasy(event.y)
        return pt_x, pt_y

    # def move_point_from(self, _):
    #     """Currently not needed"""
    #     if self.selected_points is None:
    #         return

    def move_point_to(self, event):
        """Moves a point"""
        if self.selected_points is None:
            return
        pt_x, pt_y = self.get_canvas_xy(event)
        bbox = self.canvas.coords(self.container)  # get image area
        # get real (x,y) on the image without zoom
        x_1 = round((pt_x - bbox[0]) / self.imscale)
        y_1 = round((pt_y - bbox[1]) / self.imscale)
        radius = self.radius_circle * self.imscale
        self.canvas.coords(self.selected_points,
            pt_x - radius, pt_y - radius,
            pt_x + radius, pt_y + radius)
        for obj_id in self.selected_points:
            point = self.points[obj_id]
            self.points[obj_id][0] = x_1
            self.points[obj_id][1] = y_1
            # self._app.table.item(0)["values"] = [0, 1, 2, 3]
            self._app.table.set(point[-1], 1, x_1)
            self._app.table.set(point[-1], 2, y_1)


    def add_point(self, event):
        """Adds a point"""
        if self.selected_points is not None:
            # Manipulating something else
            return
        index = len(self.points)
        if index >= len(self._app.master.reference_image_window.colors):
            # Already added all the points we can
            return
        pt_x, pt_y = self.get_canvas_xy(event)
        bbox = self.canvas.coords(self.container)  # get image area
        x_1 = round((pt_x - bbox[0]) / self.imscale)  # get real (x,y) on the image without zoom
        y_1 = round((pt_y - bbox[1]) / self.imscale)
        logger.debug(
            "Adding point at (canvas): %s %s, image: %s %s",
            pt_x, pt_y, x_1, y_1)
        # Draw sticking circle
        radius = self.radius_circle * self.imscale
        color = self._app.master.reference_image_window.colors[index],
        obj_id = self.canvas.create_oval(
            pt_x - radius, pt_y - radius,
            pt_x + radius, pt_y + radius,
            width=3,
            fill=color,
            outline=color,
            stipple="gray50",
            tags=("point",),
        )
        idd = self.point_ids[index]
        z_index = self._app.z_index
        self._app.table.insert(
            parent='',index='end', iid=idd, text='',
            values=(idd, x_1, y_1, z_index))
        self.points[obj_id] = [x_1, y_1, z_index, idd]

    # def _keypress(self, event):
    #     print("Keypress code:", event.keycode, ", state:", event.state)

    def load_image(self, data=None):
        """Loads an image"""
        if data is not None:
            self.data = data
        self.imscale = 1.0  # scale for the canvas image zoom, public for outer classes
        Image.MAX_IMAGE_PIXELS = 1000000000  # suppress DecompressionBombError for the big image
        self.__image = Image.fromarray(self.data)  # open image, but down't load it
        self.imwidth, self.imheight = self.__image.size  # public for outer classes
        self.__min_side = min(self.imwidth, self.imheight)  # get the smaller image side
        # Create image pyramid
        self.__pyramid = [Image.fromarray(self.data)]
        # Set ratio coefficient for image pyramid
        self.__curr_img = 0  # current image from the pyramid
        self.__scale = self.imscale # image pyramide scale
        self.__reduction = 2  # reduction degree of image pyramid
        width, height = self.__pyramid[-1].size
        while width > 512 and height > 512:  # top pyramid image is around 512 pixels in size
            width /= self.__reduction
            height /= self.__reduction
            self.__pyramid.append(
                    self.__pyramid[-1].resize((int(width), int(height)), self.__filter))
        # Put image into container rectangle and use it to set proper coordinates to the image
        self.container = self.canvas.create_rectangle(
            (0, 0, self.imwidth, self.imheight),
            width=0, tags=("image-bounding-box",))
        self.__show_image()  # show image on the canvas
        self.canvas.focus_set()  # set focus on the canvas

    def redraw_figures(self):
        """ Dummy function to redraw figures in the children classes """

    def grid(self, **kw):
        """ Put CanvasImage widget on the parent widget """
        self.__frame.grid(**kw)  # place CanvasImage widget on the grid
        self.__frame.grid(sticky='nswe')  # make frame container sticky
        self.__frame.rowconfigure(0, weight=1)  # make canvas expandable
        self.__frame.columnconfigure(0, weight=1)

    def tag_bind(self, *args, **kwargs):
        """Convenience wrapper around canvas tag_bind"""
        self.canvas.tag_bind(*args, **kwargs)

    def pack(self, **kw):
        """ Exception: cannot use pack with this widget """
        raise Exception('Cannot use pack with the widget ' + self.__class__.__name__)

    def place(self, **kw):
        """ Exception: cannot use place with this widget """
        raise Exception('Cannot use place with the widget ' + self.__class__.__name__)

    def __scroll_x(self, *args, **__):
        """ Scroll canvas horizontally and redraw the image """
        self.canvas.xview(*args)  # scroll horizontally
        self.__show_image()  # redraw the image

    def __scroll_y(self, *args, **__):
        """ Scroll canvas vertically and redraw the image """
        self.canvas.yview(*args)  # scroll vertically
        self.__show_image()  # redraw the image

    def __show_image(self):
        """ Show image on the Canvas. Implements correct image zoom almost like in Google Maps """
        box_image = self.canvas.coords(self.container)  # get image area
        box_canvas = (self.canvas.canvasx(0),  # get visible area of the canvas
                      self.canvas.canvasy(0),
                      self.canvas.canvasx(self.canvas.winfo_width()),
                      self.canvas.canvasy(self.canvas.winfo_height()))
        box_img_int = tuple(map(int, box_image))  # convert to integer or it will not work properly
        # Get scroll region box
        box_scroll = [min(box_img_int[0], box_canvas[0]), min(box_img_int[1], box_canvas[1]),
                      max(box_img_int[2], box_canvas[2]), max(box_img_int[3], box_canvas[3])]
        # Horizontal part of the image is in the visible area
        if  box_scroll[0] == box_canvas[0] and box_scroll[2] == box_canvas[2]:
            box_scroll[0]  = box_img_int[0]
            box_scroll[2]  = box_img_int[2]
        # Vertical part of the image is in the visible area
        if  box_scroll[1] == box_canvas[1] and box_scroll[3] == box_canvas[3]:
            box_scroll[1]  = box_img_int[1]
            box_scroll[3]  = box_img_int[3]
        # Convert scroll region to tuple and to integer
        self.canvas.configure(scrollregion=tuple(map(int, box_scroll)))  # set scroll region
        # get coordinates (x1,y1,x2,y2) of the image tie
        x_1 = max(box_canvas[0] - box_image[0], 0)
        y_1 = max(box_canvas[1] - box_image[1], 0)
        x_2 = min(box_canvas[2], box_image[2]) - box_image[0]
        y_2 = min(box_canvas[3], box_image[3]) - box_image[1]
        d_x = int(x_2 - x_1)
        d_y = int(y_2 - y_1)

        if d_x > 0 and d_y > 0:  # show image if it in the visible area
            image = self.__pyramid[max(0, self.__curr_img)].crop(  # crop current img from pyramid
                    (int(x_1 / self.__scale), int(y_1 / self.__scale),
                    int(x_2 / self.__scale), int(y_2 / self.__scale)))
            #
            imagetk = ImageTk.PhotoImage(image.resize((d_x, d_y), self.__filter))
            imageid = self.canvas.create_image(
                    max(box_canvas[0], box_img_int[0]),
                    max(box_canvas[1], box_img_int[1]),
                    anchor='nw', image=imagetk, **self.create_image_kwargs)
            self.canvas.lower(imageid)  # set image into background
            self.canvas.lower(self.container)  # set hidden bbox even lower
            self.canvas.imagetk = imagetk  # keep an extra reference to prevent garbage-collection

    def __move_from(self, event):
        """ Remember previous coordinates for scrolling with the mouse """
        self.canvas.scan_mark(event.x, event.y)

    def __move_to(self, event):
        """ Drag (move) canvas to the new position """
        self.canvas.scan_dragto(event.x, event.y, gain=1)
        self.__show_image()  # zoom tile and show it on the canvas

    def outside(self, pos_x, pos_y):
        """ Checks if the point (x,y) is outside the image area """
        bbox = self.canvas.coords(self.container)  # get image area
        if bbox[0] < pos_x < bbox[2] and bbox[1] < pos_y < bbox[3]:
            return False  # point (x,y) is inside the image area
        return True  # point (x,y) is outside the image area

    def __wheel(self, event):
        """ Zoom with mouse wheel """
        pos_x = self.canvas.canvasx(event.x)  # get coordinates of the event on the canvas
        pos_y = self.canvas.canvasy(event.y)
        if self.outside(pos_x, pos_y):
            return  # zoom only inside image area
        scale = 1.0
        # Respond to Linux (event.num) or Windows (event.delta) wheel event
        if event.num == 5 or event.delta == -120:  # scroll down, smaller
            if round(self.__min_side * self.imscale) < 30:
                return  # image is less than 30 pixels
            self.imscale /= self.__delta
            scale        /= self.__delta
        if event.num == 4 or event.delta == 120:  # scroll up, bigger
            fact = min(self.canvas.winfo_width(), self.canvas.winfo_height()) >> 1
            if fact < self.imscale:
                return  # 1 pixel is bigger than the visible area
            self.imscale *= self.__delta
            scale        *= self.__delta
        # Take appropriate image from the pyramid
        self.__curr_img = min(
                -1 * int(math.log(self.imscale, self.__reduction)),
                len(self.__pyramid) - 1)
        self.__scale = self.imscale * math.pow(self.__reduction, max(0, self.__curr_img))
        #
        self.canvas.scale('all', pos_x, pos_y, scale, scale)  # rescale all objects
        # Redraw some figures before showing image on the screen
        self.redraw_figures()  # method for child classes
        self.__show_image()

    def _keypress(self, event):
        """ Scrolling with the keyboard.
            Independent from the language of the keyboard, CapsLock, <Ctrl>+<key>, etc. """
        keycode = event.keycode
        symbol = event.keysym
        logger.debug(
            "Keypress code: %s, state: %s, sym: %s, symnum: %s, char: %s",
            keycode, event.state, symbol, event.keysym_num, event.char)
        # print(dir(event))
        if event.state - self.__previous_state == 4:
            # means that the Control key is pressed
            return # do nothing if Control key is pressed
        self.__previous_state = event.state  # remember the last keystroke state
        # Up, Down, Left, Right keystrokes
        if keycode in [68, 39, 102]:  # scroll right: keys 'D', 'Right' or 'Numpad-6'
            self.__scroll_x('scroll',  1, 'unit', event=event)
        elif keycode in [65, 37, 100]:  # scroll left: keys 'A', 'Left' or 'Numpad-4'
            self.__scroll_x('scroll', -1, 'unit', event=event)
        elif keycode in [87, 38, 104]:  # scroll up: keys 'W', 'Up' or 'Numpad-8'
            self.__scroll_y('scroll', -1, 'unit', event=event)
        elif keycode in [83, 40, 98]:  # scroll down: keys 'S', 'Down' or 'Numpad-2'
            self.__scroll_y('scroll',  1, 'unit', event=event)
        elif symbol == "h":
            state = self.canvas.itemconfigure("point")["state"][4]
            if state == "hidden":
                self.canvas.itemconfigure("point", state='normal')
            else:
                self.canvas.itemconfigure("point", state='hidden')


    def crop(self, bbox):
        """ Crop rectangle from the image and return it """
        return self.__pyramid[0].crop(bbox)

    def destroy(self):
        """ ImageFrame destructor """
        self.__image.close()
        map(lambda i: i.close, self.__pyramid)  # close all pyramid images
        del self.__pyramid[:]  # delete pyramid list
        del self.__pyramid  # delete pyramid variable
        self.canvas.destroy()
        self.__frame.destroy()


class AlignWindow(ttk.Panedwindow):  #
    """Main alignment window class"""
    def __init__(
                self,
                root,
                width,
                height,
                data=None,
                files=None,
                channel=None):
        super().__init__(root, orient=tk.HORIZONTAL)
        logger.debug("Data: %s, shape: %s", type(data), getattr(data, 'shape', 'Has no shape'))

        self.data = data
        self.reader = None
        self.files = files
        self.channel = channel
        self.num_channels = None
        self.z_index = 0
        self.grid(
            column=0, row=0,
            sticky= (tk.N + tk.S + tk.E + tk.W))
        self.menu = tk.Menu(root)
        self.filemenu = tk.Menu(self.menu, tearoff=False)
        self.datamenu = tk.Menu(self.menu, tearoff=False)
        self.filemenu.add_command(
            label="Load data",
            command=self.select_data_files)
        self.filemenu.add_command(
            label="Save points",
            command=self.save_points)
        self.datamenu.add_command(
            label="Select Channel",
            command=self.select_channel)
        self.menu.add_cascade(label="File", menu=self.filemenu)
        self.menu.add_cascade(label="Data", menu=self.datamenu, state="disabled")
        root.configure(menu=self.menu)

        # two panes, each of which would get widgets gridded into it:
        self.imageframe = ttk.Labelframe(self, text='Data', width=width // 2, height=height)
        self.imageframe.grid(
            column=0, row=0,
            sticky= (tk.N + tk.S + tk.E + tk.W))
        self.imageframe.grid_columnconfigure(0,weight=1)
        self.imageframe.grid_rowconfigure(0,weight=1)
        self.canvas = CanvasImage(self.imageframe, data[0], app=self, tags=("image",))
        self.canvas.grid(
            column=0, row=0,
            sticky= (tk.N + tk.S + tk.E + tk.W))

        self.z_slider = ttk.Scale(
            self.imageframe,
            from_=0,
            to=len(data)-1,
            command=self.update_zindex,
            orient="vertical")
        self.z_slider.grid(column=0, row=0, sticky=(tk.N + tk.S + tk.W))

        tableframe = ttk.Labelframe(self, text='Points')
        tableframe.grid(
            column=0, row=0,
            sticky= (tk.N + tk.S + tk.E + tk.W))
        self.table = ttk.Treeview(tableframe)
        self.table.grid(
            column=0, row=0,
            sticky= (tk.N + tk.S + tk.E + tk.W))
        self.add_table_data()
        self.add(self.imageframe)
        self.add(tableframe)

        tableframe.grid_columnconfigure(0, weight=1)
        tableframe.grid_rowconfigure(0, weight=1)
        if files is not None:
            self.load_data(files)

    # def add_table_data(self, data=None):
    def add_table_data(self):
        """ Adds the data table"""
        self.table['columns'] = ('point_id', 'point_x', 'point_y', 'point_z')

        self.table.column("#0", width=0,  stretch=tk.NO)
        self.table.column("point_id",anchor=tk.CENTER, width=80)
        self.table.column("point_x",anchor=tk.CENTER,width=80)
        self.table.column("point_y",anchor=tk.CENTER,width=80)
        self.table.column("point_z",anchor=tk.CENTER,width=80)

        self.table.heading("#0",text="",anchor=tk.CENTER)
        self.table.heading("point_id",text="Id",anchor=tk.CENTER)
        self.table.heading("point_x",text="X",anchor=tk.CENTER)
        self.table.heading("point_y",text="Y",anchor=tk.CENTER)
        self.table.heading("point_z",text="Z",anchor=tk.CENTER)

    def update_zindex(self, index):
        """Updates z-index to value given"""
        index=int(round(float(index)))
        self.z_index = index
        logger.debug("Selecting Z-index %s", index)
        self.canvas.load_image(self.data[index])

    def load_data(self, files):
        """Loads data in files"""
        if len(files) == 0:
            logger.info("No files given, not loading data")
            return
        # self.files = sorted([str(f) for f in dlg.selectedFiles()])
        # logger.info("Files:")
        # logger.info("%s", files)
        self.files = files
        self.reader = zio.Reader(
            files,
            sample_factor=(1, 1),
            align_on_load=True,
            use_cache=True,
            channel=self.channel,
            t_end=5,
        )

        self.z_index = 0
        self.update_data_from_reader()

    def update_data_from_reader(self):
        logger.debug("Loading data from reader...")
        if self.reader.ndim == 2:
            # 2D Registration
            self.data = np.array([self.reader[0]])
            self.is_2d = True
        elif self.reader.ndim == 3 and self.reader.num_channels is not None:
            self.data = np.array([self.reader[0]])
            self.is_2d = True
        elif self.reader.ndim == 3 and self.reader.num_channels is None:
            self.is_2d = False
            self.data = self.reader[0]
        elif self.reader.ndim == 4:
            # logger.info("Performing maximum projection...")
            # self.data = self.reader.max(axis=0).transpose([2, 0, 1])
            self.data = self.reader[0] # .max(axis=0).transpose([2, 0, 1])
        else:
            logger.error("UNKNOWN FORMAT %s %s", self.data.shape, self.data.ndim)
            raise ValueError(f"Unsupported data dimensionality: {self.data.ndim}")
        data_min, data_max = self.data.min(), self.data.max()
        logger.debug("Data is shape: %s, min: %s, max: %s, dtype: %s",
                     self.data.shape, data_min, data_max, self.data.dtype)
        if (0 > data_min) or (data_min > 100) or (100 > data_max) or (data_max > 255):
            logger.info("Poor contrast data, normalizing")
            self.data = 255 * (self.data.astype(float) - data_min) / (data_max - data_min)
            data_min, data_max = self.data.min(), self.data.max()
            logger.debug("Done normalisation, data is shape: %s, min: %s, max: %s, dtype: %s",
                         self.data.shape, data_min, data_max, self.data.dtype)

        self.update_z_slider()
        self.data_updated()

    def update_z_slider(self):
        z_max = len(self.data) - 1
        if z_max > 1:
            self.z_slider.configure(to=z_max)
            self.z_slider.grid(column=0, row=0, sticky=(tk.N + tk.S + tk.W))
        else:
            self.z_slider.grid_forget()

    def select_data_files(self):
        """Uses a standard dialog to select files"""
        filenames = get_existing_filenames()
        self.load_data(filenames)

    def select_channel(self):
        """Uses a standard dialog to select channel"""
        prompt = f'Select channel (0  - {self.reader.num_channels - 1}):'
        channel = simpledialog.askinteger("Select channel", prompt)
        if channel is not None:
            self.reader.change_channel(channel)
            self.update_data_from_reader()

    def save_points(self):
        """Saves the point information"""
        savedir = Path(f'{self.files[0]}_ROIS')
        if not savedir.is_dir():
            savedir.mkdir()

        savefile = savedir / "rois.txt"
        with savefile.open(encoding='utf8', mode='w') as fid:
            for point in self.canvas.points.values():
                fid.write("{}\n".format(
                        ",".join([point[-1]] + [f"{val:0.2f}" for val in point[:-1]])))

    def data_updated(self):
        """Data has been updated, do these things"""
        logger.info("Calling data_updated")
        self.canvas.load_image(self.data[self.z_index])
        self.menu.entryconfig("Data", state="normal")
        if self.reader.num_channels is None:
            self.datamenu.entryconfig("Select Channel", state="disabled")
        elif self.reader.num_channels == 1:
            self.datamenu.entryconfig("Select Channel", state="disabled")
        else:
            self.datamenu.entryconfig("Select Channel", state="normal")
