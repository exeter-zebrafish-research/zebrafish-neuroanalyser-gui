# -*- coding: utf-8 -*-
"""Tk-based main running app

J. Metz <metz.jp@gmail.com>
"""
import logging
from concurrent.futures import ThreadPoolExecutor
import traceback

import tkinter as tk
from tkinter import filedialog, scrolledtext
from  tkinter import ttk

import zebrafish_neuroanalyser.main
# s = ttk.Style()
# print(s.theme_names())
# s.theme_use('')
# print(s.theme_use(''))

logger = logging.getLogger(__name__)


def run(files=(),  # pylint: disable=unused-argument
        dev=False,
        moviewriter=None,
        output_path_folder="output",
        selected_rois_file=None,
        **__):
    # """
    # Runs the application
    # """
    app = Application(files=files)
    app.run()


def debug_printer(*args, **kwargs):
    print("DEBUG PRINT:")
    for arg in args:
        print("   ", arg)
    for name, value in kwargs.items():
        print("   ", name, ":", value)


class Application(tk.Tk):
    """
    Root TK application, creates root object and AlignWindow
    """
    def __init__(self, files=()):
        # self.style = ttk.Style(self)
        super().__init__()
        self.files = files
        self.worker_thread = None
        self._executor = ThreadPoolExecutor(max_workers=1)
        self.grid()
        self._setup_content()

    def run(self):
        """Run the main loop"""
        self.mainloop()

    def _setup_content(self):
        self.title('Zebrafish-Neuroanalyser Aligner')

        self._add_menu()

        # root.tk.call('source', 'breeze-dark.tcl')
        # self.grid()
        # ws['bg'] = '#AC99F2'
        # self.grid_columnconfigure(0, weight=1)
        # self.grid_rowconfigure(0, weight=1)
        # self.resizable(True, True)
        # # create the sizegrip
        # size_grip = ttk.Sizegrip(self)
        # size_grip.grid(row=1, sticky=tk.SE)
        self.columnconfigure(0, weight=1)
        self.frame_input = InputFrame(
                self,
                files=self.files,
                log_func=self.log,
                set_runnable_func=self.set_runnable_state)

        if False:
            frame_analysis = ttk.Labelframe(self, text='Analysis')
            frame_analysis.grid(column=0, row=1, sticky= tk.NSEW)
            tk.Label(frame_analysis, text="Grid Size (px)").grid(row=0, column=0, sticky=tk.NSEW)
            self.spin_grid_size = tk.Spinbox(frame_analysis, value=3)
            self.spin_grid_size.grid(row=0, column=1, sticky=tk.NSEW)
            frame_analysis.rowconfigure(0, weight=1)
            frame_analysis.columnconfigure(0, weight=1)
            frame_analysis.columnconfigure(1, weight=1)

        # self.rowconfigure(1, weight=2)

        frame_output = ttk.Labelframe(self, text='Output')
        frame_output.grid(column=0, row=2, sticky= tk.NSEW)
        self.output_entry = tk.Entry(frame_output)
        if len(self.files) == 0:
            self.output_entry.insert(-1, "Unset")
            tk.Button(frame_output, state=tk.DISABLED).grid(row=0, column=1, sticky=tk.NSEW)
        else:
            self.output_entry.insert(-1, f"{self.files[0]}_outputs")
            tk.Button(frame_output, text="Change").grid(row=0, column=1, sticky=tk.NSEW)
        self.output_entry.configure(state=tk.DISABLED)
        self.output_entry.grid(row=0, column=0, sticky=tk.NSEW)

        frame_output.rowconfigure(0, weight=1)
        frame_output.columnconfigure(0, weight=4)
        frame_output.columnconfigure(1, weight=1)

        # self.rowconfigure(2, weight=2)

        frame_log = ttk.Labelframe(self, text='Log')
        frame_log.grid(row=3, column=0, sticky=tk.NSEW)
        self.entry_log = scrolledtext.ScrolledText(frame_log, state=tk.DISABLED)
        self.entry_log.grid(row=0, column=0, sticky=tk.NSEW)
        frame_log.columnconfigure(0, weight=1)
        frame_log.rowconfigure(0, weight=1)
        self.rowconfigure(3, weight=1)

        if len(self.files) == 0:
            self.btn_start = tk.Button(
                    self, text="Run", # text="Select input files first",
                    state=tk.DISABLED,
                    command=self.run_processing,
                    )
            self.log("Starting AlignGui")
            self.log("Select files, set settings, then click 'run'")
        else:
            self.btn_start = tk.Button(
                    self, text="Run", command=self.run_processing)
            self.log(f"Starting AlignGui with {len(self.files)} input files")

        self.btn_start.grid(row=4, column=0, sticky=(tk.EW + tk.S))

    def _add_menu(self):
        self.menu = tk.Menu(self)
        self.helpmenu = tk.Menu(self.menu, tearoff=False)
        self.helpmenu.add_command(
            label="About...",
            command=self.show_about)
        self.menu.add_cascade(label="Help", menu=self.helpmenu)
        self.configure(menu=self.menu)

    def show_about(self):
        tk.messagebox.showinfo("About Zebrafish-Neuroanalyser",
                    "This is the more portable Tk version of the launcher")

    def run_processing(self):
        """Starts a run in a worker thread"""
        # self.worker_thread =
        self.log("Starting processing...")
        self.btn_start.configure(command = self.cancel_processing, text="Cancel")
        self.worker = self._executor.submit(
            zebrafish_neuroanalyser.main.main,
            filenames=self.frame_input.files,
            output_path=self.output_entry.get(),#  / configure('text'),
            # output_types=['png'],
            # n_clusters=10,
            # n_lines=1000,
            align_frames=self.frame_input.check_align.instate(["selected"]), # self.,
            sample_factor=int(self.frame_input.spin_resample.get()),
            # write_film=True,
            # reference_rois_file=None,
            # selected_rois_file=None,
            # roifilearchive=None,
            # roifiledir=os.path.join(PWD, "labels", "individual_labels"),
            # roifilebase="%06d.tif",
            start_frame=int(self.frame_input.spin_start_frame.get()),
            end_frame=int(self.frame_input.spin_end.get()),
            # n_rois=None,
            # moviewriter=None,
            channel=int(self.frame_input.spin_channel.get()) - 1,
            # use_cache=True,
            # clear_cache=False,
        )
        self.worker.add_done_callback(self.completed_processing)

    def completed_processing(self, future=None):
        print("completed thing:", future)
        exc = future.exception()
        if exc is not None:
            self.log("Processing failed. Exception was:", level=logging.ERROR)
            self.log("\n".join(traceback.format_exception(exc)), level=logging.ERROR)
        else:
            self.log("Processing completed!")
        self.btn_start.configure(command = self.run_processing, text="Run")

    def set_runnable_state(self, state):
        if state:
            self.btn_start.configure(state=tk.NORMAL)
        else:
            self.btn_start.configure(state=tk.DISABLED)

    def cancel_processing(self):
        """Attempt to cancel running thread"""
        self.log("Processing cancelled.")
        self.btn_start.configure(command = self.run_processing, text="Run")

    def log(self, message, level=logging.INFO):
        logger.log(level, message)
        self.entry_log.configure(state=tk.NORMAL)
        message = message.rstrip() + "\n"
        self.entry_log.insert(tk.INSERT, message)
        self.entry_log.configure(state=tk.DISABLED)





class InputFrame(tk.LabelFrame):
    files: list[str] = []

    def __init__(
            self,
            *args,
            files=None,
            text="Input",
            log_func=None,
            set_runnable_func=None,
            **kwargs):
        super().__init__(*args, text=text, **kwargs)
        # frame_input = ttk.Labelframe(self, text='Input')
        # main_frame.grid_columnconfigure(0,weight=1)
        # main_frame.grid_rowconfigure(0,weight=1)
        button_select = tk.Button(self, text="Select", command=self.select_files)
        button_select.grid(column=0, row=0, sticky=tk.NSEW)
        if log_func is not None:
            self.log = log_func
        else:
            self.log = lambda _: None
        if set_runnable_func is None:
            self.set_runnable_func = lambda _: None
        else:
            self.set_runnable_func = set_runnable_func

        if files is None or len(files) == 0:
            self.label_files= tk.Label(self, text="No file selected")
        else:
            self.files = list(files)
            text = self.files[0]
            if len(files) > 1:
                text += f"(and {len(files)-1} more files)"
            self.label_files= tk.Label(self, text=text)
        self.label_files.grid(column=1, row=0, sticky=tk.NSEW, columnspan=9)
        label_start = tk.Label(self, text="Start frame")
        label_start.grid(column=0, row=1, sticky=tk.NSEW)
        self.spin_start_frame = tk.Spinbox(self, value=0)
        self.spin_start_frame.grid(column=1, row=1, sticky=tk.NSEW)
        label_end = tk.Label(self, text="End frame")
        label_end.grid(column=2, row=1, sticky=tk.NSEW)
        self.spin_end = tk.Spinbox(self, value=0)
        self.spin_end.grid(column=3, row=1, sticky=tk.NSEW)

        label_resample = tk.Label(self, text="Resample factor")
        label_resample.grid(column=4, row=1, sticky=tk.NSEW)
        self.spin_resample = tk.Spinbox(self, value=3)
        self.spin_resample.grid(column=5, row=1, sticky=tk.NSEW)

        label_channel = tk.Label(self, text="Channel")
        label_channel.grid(column=6, row=1, sticky=tk.NSEW)
        self.spin_channel = tk.Spinbox(self, value=1)
        self.spin_channel.grid(column=7, row=1, sticky=tk.NSEW)


        label_align = tk.Label(self, text="Align frames")
        label_align.grid(column=8, row=1, sticky=tk.NSEW)
        self.check_align = ttk.Checkbutton(self)
        self.check_align.grid(column=9, row=1, sticky=tk.NSEW)
        self.check_align.state(['!alternate'])
        self.check_align.state(['selected'])

        # self.grid(column=0, row=0, sticky= (tk.N + tk.E + tk.W))
        self.grid(column=0, row=0, sticky= tk.NSEW)
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        for ind in range(8):
            self.columnconfigure(ind, weight=1)
        self.togglable = (
                label_start,
                self.spin_start_frame,
                label_end,
                self.spin_end,
                label_resample,
                self.spin_resample,
                label_align,
                self.check_align,
        )
        if files is None or len(files) == 0:
            self.disable_options()

        # self.rowconfigure(0, weight=2)

    def select_files(self):
        files = get_existing_filenames()
        if files:
            self.files = files
            self.enable_options()
            self.label_files.config(text=", ".join(self.files))
            self.log(f"Selected {len(files)} files")
            self.set_runnable_func(True)
        else:
            self.log("No new files selected")
            if len(self.files) == 0:
                self.disable_options()
                self.set_runnable_func(False)

    def enable_options(self):
        for item in self.togglable:
            item.config(state=tk.NORMAL)

    def disable_options(self):
        for item in self.togglable:
            item.config(state=tk.DISABLED)





def get_existing_directory(**tk_options):
    """Use standard TK to get an existing directory"""
    return filedialog.askdirectory(**tk_options)


def get_existing_filenames(**tk_options):
    """Use standard TK to get an existing filenames"""
    return filedialog.askopenfilenames(**tk_options)
