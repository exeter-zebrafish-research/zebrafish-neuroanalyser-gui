# -*- coding: utf-8 -*-
"""QT-based GUI for launching main processing

J. Metz <metz.jp@gmail.com>
"""
from pathlib import Path
import sys
import os
import tempfile
import time
import subprocess
from PyQt5 import QtGui, QtCore, QtWidgets  # type: ignore

import zebrafish_neuroanalyser.main
from zebrafish_neuroanalyser.utility import sort_filenames, trunc

# =========================================================
# Main GUI classes
# =========================================================
# pylint: disable=c-extension-no-member

class Worker(QtCore.QThread):  # pylint: disable=too-few-public-methods,too-many-instance-attributes
    """Worker thread to run processing and leave GUI responsive"""
    completed_run = QtCore.pyqtSignal()
    update = QtCore.pyqtSignal(int, int, str)

    def __init__(self, main_func, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.main_func = main_func
        self.files = []
        self.output_path = None
        self.align_frames = None
        self.sample_factor = 3
        self.start_frame = 0
        self.end_frame = 0
        self.moviewriter = None
        self.selected_rois_file = None

    def run(self):
        """Main thread run function, starts the processing"""

        def update_progress(current=-1, total=-1, message=""):
            self.update.emit(current, total, message)

        def show_error(err):
            QtWidgets.QMessageBox.critical(self, "Critical", err)

        self.main_func(
            filenames=self.files,
            update_function=update_progress,
            error_message_function=show_error,
            output_path=self.output_path,
            align_frames=self.align_frames,
            sample_factor=[self.sample_factor, self.sample_factor],
            start_frame=self.start_frame,
            end_frame=self.end_frame,
            moviewriter=self.moviewriter,
            selected_rois_file=self.selected_rois_file,
        )
        self.completed_run.emit()


class MainDialog(QtWidgets.QDialog):  # pylint: disable=too-many-instance-attributes
    """Main dialog showing inputs for the run function"""
    def __init__(
            self,
            main_func,
            output_path_folder=Path("output")):  # pylint: disable=too-many-statements
        super().__init__()

        # Create widgets and layouts
        self.layout = QtWidgets.QVBoxLayout()
        # Info
        self.label = QtWidgets.QLabel("")

        # Input section
        self.lbl_input_files = QtWidgets.QLabel("No data selected")
        self.btn_select_data = QtWidgets.QPushButton("Select")
        if QtGui.QIcon.hasThemeIcon("document-open"):
            self.btn_select_data.setIcon(QtGui.QIcon.fromTheme("document-open"))
        self.chk_align_frames = QtWidgets.QCheckBox("Align frames")

        # Output section
        self.lbl_output = QtWidgets.QLabel("Output:")
        self.btn_select_output = QtWidgets.QPushButton("Change")
        if QtGui.QIcon.hasThemeIcon("folder"):
            self.btn_select_output.setIcon(QtGui.QIcon.fromTheme("folder"))
        self.line_output_display = QtWidgets.QLineEdit()

        # Progress
        self.pbar = QtWidgets.QProgressBar()
        # Log
        self.log = QtWidgets.QTextEdit()
        # Start/cancel
        self.btn_start_cancel = QtWidgets.QPushButton("Select input files first")

        self.lbl_start_frame = QtWidgets.QLabel('Start frame')
        self.edt_start_frame = QtWidgets.QSpinBox()
        self.lbl_end_frame = QtWidgets.QLabel('End frame')
        self.edt_end_frame = QtWidgets.QSpinBox()
        self.lbl_sample_size = QtWidgets.QLabel('Resample factor')
        self.edt_sample_size = QtWidgets.QSpinBox()

        self.lbl_analysis_grid_size = QtWidgets.QLabel('Grid Size (pixels)')
        self.edt_analysis_grid_size = QtWidgets.QSpinBox()

        self.input_layout = QtWidgets.QGridLayout()
        self.input_layout.addWidget(self.lbl_input_files, 0, 0, 4, 0)
        self.input_layout.addWidget(self.btn_select_data, 0, 5, 1, 1)
        self.input_layout.addWidget(self.chk_align_frames, 1, 5, 1, 1)
        self.input_layout.addWidget(self.lbl_start_frame, 2, 0, 1, 1)
        self.input_layout.addWidget(self.edt_start_frame, 2, 1, 1, 1)
        self.input_layout.addWidget(self.lbl_end_frame, 2, 2, 1, 1)
        self.input_layout.addWidget(self.edt_end_frame, 2, 3, 1, 1)

        self.input_layout.addWidget(self.lbl_sample_size, 2, 4, 1, 1)
        self.input_layout.addWidget(self.edt_sample_size, 2, 5, 1, 1)

        self.analysis_layout = QtWidgets.QGridLayout()
        self.analysis_layout.addWidget(self.lbl_analysis_grid_size, 0, 0)
        self.analysis_layout.addWidget(self.edt_analysis_grid_size, 0, 1)

        self.output_layout = QtWidgets.QHBoxLayout()
        self.output_layout.addWidget(self.line_output_display)
        self.output_layout.addWidget(self.btn_select_output)

        self.grp_output = QtWidgets.QGroupBox("Output")
        self.grp_output.setLayout(self.output_layout)
        self.grp_analysis = QtWidgets.QGroupBox("Analysis")
        self.grp_analysis.setLayout(self.analysis_layout)
        self.grp_input = QtWidgets.QGroupBox("Input")
        self.grp_input.setLayout(self.input_layout)

        self.layout.addWidget(self.label)
        self.layout.addWidget(self.grp_input)
        self.layout.addWidget(self.grp_analysis)
        self.layout.addWidget(self.grp_output)
        self.layout.addWidget(self.pbar)
        self.layout.addWidget(self.log)
        self.layout.addWidget(self.btn_start_cancel)

        # Initialize states
        self.btn_start_cancel.setEnabled(False)
        self.chk_align_frames.setChecked(True)

        self.lbl_start_frame.setEnabled(False)
        self.lbl_end_frame.setEnabled(False)
        self.edt_start_frame.setEnabled(False)
        self.edt_end_frame.setEnabled(False)
        self.edt_start_frame.setMinimum(0)
        self.edt_start_frame.setMaximum(1000)
        self.edt_end_frame.setMinimum(0)
        self.edt_end_frame.setMaximum(1000)

        self.edt_sample_size.setValue(3)
        self.edt_analysis_grid_size.setMinimum(1)
        self.edt_analysis_grid_size.setMaximum(512)
        self.edt_analysis_grid_size.setValue(32)
        self.pbar.hide()

        self.pbar.setMaximum(0)
        self.pbar.setMinimum(0)

        self.log.setReadOnly(True)
        self.line_output_display.setReadOnly(True)

        self.setLayout(self.layout)

        # Add in associated variables
        self.files = []
        self.output_path = ""
        self.output_path_folder = output_path_folder
        self.progress_time_started = None

        self.resize(480, 300)
        self.setWindowTitle("Data Processing")
        self.prog_base_message = "Processing image stack..."

        self.worker = Worker(main_func)
        # Connect up signals and slots
        self.connect_signals_slots()

    def connect_signals_slots(self):
        """Connects all signals to slots"""
        self.worker.finished.connect(self.terminated)
        self.worker.completed_run.connect(self.completed_run)
        self.worker.update.connect(self.progress)
        self.btn_start_cancel.clicked.connect(self.startcancel)
        self.btn_select_data.clicked.connect(self.set_filenames)
        self.btn_select_output.clicked.connect(self.select_output)
        self.edt_start_frame.valueChanged.connect(self.edt_end_frame.setMinimum)
        self.edt_end_frame.valueChanged.connect(self.edt_start_frame.setMaximum)

    def select_output(self):
        """Select the output path"""
        output_path_str = str(
                QtWidgets.QFileDialog.getExistingDirectory(
                    None,
                    "Select output directory", self.output_path))
        self.line_output_display.set_text(trunc(output_path_str))
        self.add_log(f"Output changed to : {trunc(output_path_str)}")
        self.output_path = Path(output_path_str)

    def set_filenames(self):
        """Set the input filenames using a dialog"""
        dlg = QtWidgets.QFileDialog(self)
        dlg.setWindowTitle("Select data")
        dlg.setFileMode(QtWidgets.QFileDialog.ExistingFiles)
        if dlg.exec_():
            # self.files = sorted([str(f) for f in dlg.selectedFiles()])
            files = [str(f) for f in dlg.selectedFiles()]
            self.files = sort_filenames(files, time_regex=r"TL(\d+)")

            if len(self.files) == 0:
                self.lbl_input_files.setText("No input selected")
                self.add_log("No input files selected")
                self.lbl_start_frame.setEnabled(False)
                self.lbl_end_frame.setEnabled(False)
                self.edt_start_frame.setEnabled(False)
                self.edt_end_frame.setEnabled(False)
                return
            if len(self.files) == 1:
                self.lbl_input_files.setText(os.path.basename(self.files[0]))
                self.add_log(f"One input file selected : {trunc(self.files[0])}")
                self.lbl_start_frame.setEnabled(True)
                self.lbl_end_frame.setEnabled(True)
                self.edt_start_frame.setEnabled(True)
                self.edt_end_frame.setEnabled(True)
            else:
                self.lbl_input_files.setText(f"{len(self.files)} files selected")
                self.add_log(f"{len(self.files)} input files selected:")
                if len(self.files) < 5:
                    for name in self.files:
                        self.add_log(f"- {trunc(name)}")
                else:
                    for name in self.files[:2]:
                        self.add_log(f"- {trunc(name)}")
                    self.add_log("  :")
                    for name in self.files[-2:]:
                        self.add_log(f"- {trunc(name)}")
                self.lbl_start_frame.setEnabled(True)
                self.lbl_end_frame.setEnabled(True)
                self.edt_start_frame.setEnabled(True)
                self.edt_end_frame.setEnabled(True)
                self.edt_start_frame.setMaximum(len(self.files)-1)
                self.edt_end_frame.setMaximum(len(self.files)-1)
                self.edt_start_frame.setValue(0)
                self.edt_end_frame.setValue(len(self.files)-1)

            infiledir = os.path.dirname(self.files[0])
            self.output_path = Path(infiledir, self.output_path_folder)
            self.line_output_display.setText(trunc(str(self.output_path)))
            if not self.btn_start_cancel.isEnabled():
                self.btn_start_cancel.setEnabled(True)
                self.btn_start_cancel.setText("Start")

    def set_value(self, *args, **kwargs):
        """Set current progress-bar value"""
        self.pbar.setValue(*args, **kwargs)

    def set_maximum(self, *args, **kwargs):
        """Set maximum on progress bar"""
        self.pbar.setMaximum(*args, **kwargs)

    def set_minimum(self, *args, **kwargs):
        """Set minimum on progress bar"""
        self.pbar.setMinimum(*args, **kwargs)

    def set_label_text(self, *args, **kwargs):
        """Set label text"""
        self.label.setText(*args, **kwargs)

    def add_log(self, message):
        """Adds a log message"""
        self.log.append(message)

    def terminated(self):
        """Clean up when a job is aborted"""
        self.pbar.hide()
        self.set_label_text("ABORTED")
        self.add_log('Processing ABORTED (worker thread terminated)')
        self.btn_start_cancel.setText("Start")

    def completed_run(self):
        """Clean up once run completes"""
        self.set_label_text("Finished processing!")
        self.btn_start_cancel.setText("Start")
        self.pbar.hide()
        self.add_log('Finished processing!')
        launch_file_explorer(self.worker.output_path, self)

    def progress(self, current=-1, total=-1, message=""):
        """Format the progress bar"""
        if current > -1:
            if self.progress_time_started is None:
                self.progress_time_started = time.time()
            if (current > 0) and (total > -1):
                t_taken = time.time() - self.progress_time_started
                t_left = ((float(total) / current) - 1)*t_taken
                self.pbar.setFormat(f"{100*current/total:d} % - {t_left:d}s remaining")
            self.set_value(current)
            if total > -1:
                self.set_maximum(total)
        else:
            self.set_maximum(0)
            self.set_minimum(0)
            # reset the timer
            self.progress_time_started = None

        if message:
            self.set_label_text(f"{self.prog_base_message}\n{message}")
            self.add_log(message)
            # And still output to console
            print(message)

    def startcancel(self):
        """Start or concel a run"""
        if self.worker.isRunning():
            self.worker.terminate()
        else:
            self.pbar.show()
            self.btn_start_cancel.setText("Cancel")
            # worker.files = prog.files[
            #    prog.edtStartFrame.value() : prog.edtEndFrame.value()+1]
            self.worker.files = self.files
            self.worker.start_frame = self.edt_start_frame.value()
            self.worker.end_frame = self.edt_end_frame.value()
            self.worker.output_path = self.output_path
            self.worker.align_frames = self.chk_align_frames.isChecked()
            self.worker.sample_factor = self.edt_sample_size.value()
            self.worker.start()


#
# Additional support functions
#

def launch_file_explorer(path, parent=None):
    """Launches file explorer / finder"""
    # Try cross-platform file-explorer opening...
    # Courtesy of: http://stackoverflow.com/a/1795849/537098
    if sys.platform == 'win32':
        subprocess.Popen(['start', path], shell=True)  # pylint: disable=consider-using-with
    elif sys.platform == 'darwin':
        subprocess.Popen(['open', path])  # pylint: disable=consider-using-with
    else:
        try:
            subprocess.Popen(['xdg-open', path])  # pylint: disable=consider-using-with
        except OSError:
            # er, think of something else to try
            # xdg-open *should* be supported by recent Gnome, KDE, Xfce
            QtWidgets.QMessageBox.critical(
                parent,
                "Oops",
                "Couldn't launch the file explorer, sorry!\n"
                f"Manually open {path} in your favourite file manager")


def get_existing_folder_gui(message : str ="Select directory") -> str:
    """Ask for an existing folder"""
    app = QtWidgets.QApplication.instance()
    if app is None:
        app = QtWidgets.QApplication([])
    rootpath = QtWidgets.QFileDialog.getExistingDirectory(
        None, message, "")
    return str(rootpath)


# -----------------------------
# Main entry point
# -----------------------------


def run(
        main_func=None,
        # filenames=(),  # pylint: disable=unused-argument
        dev=False,
        moviewriter=None,
        output_path_folder=Path("output"),
        selected_rois_file=None,
        **__):
    """Main run function to show the dialog"""
    app = QtWidgets.QApplication(sys.argv)  # pylint: disable=c-extension-no-member
    app.setStyle("plastique")
    if main_func is None:
        main_func = zebrafish_neuroanalyser.main.main

    prog = MainDialog(main_func, output_path_folder=output_path_folder)
    if dev:
        prog.files = ["dev", ]
        prog.lbl_input_files.setText("dev")
        prog.btn_start_cancel.setEnabled(True)
        prog.edt_sample_size.setValue(1)
        prog.output_path = Path(tempfile.gettempdir(), os.path.basename(__file__))
        prog.btn_start_cancel.setText("Run test data")

    # Pass parameters to the worker
    prog.worker.moviewriter = moviewriter
    prog.worker.selected_rois_file = selected_rois_file
    prog.show()

    app.exec_()
    prog.worker.terminate()
