import logging
import argparse

from zebrafish_neuroanalyser.utility import set_loggers_to_debug
from zebrafish_neuroanalyser_gui import (
        run_gui_classical,
        run_gui_align,
        run_gui_legacy,
        utility,
        version,
)

logger = logging.getLogger(__name__)


def create_parser():
    """
    Creates a parser object with the required flags and arguments
    """
    # parser.add_argument('--web', action='store_true', default=False)
    # parser.add_argument('--legacy', action='store_true', help="Use legacy code where available")
    parser = argparse.ArgumentParser(
        __package__,
        description='Zebrafish Neuroanalyser GUI (Graphical User Interface)')
    parser.add_argument('--version', '-V', action='store_true', help="Get version info")
    subparsers = parser.add_subparsers(help="Command to run", required=False)
    parser_align = subparsers.add_parser('align', help='Run alignment GUI')
    parser_align.add_argument("files", nargs="*", help="File(s) to align")
    parser_align.add_argument(
            "--channel",
            help="Channel to display if multischannel image",
            type=int, default=0)
    # parser_align.add_argument("region", help="Region to scrape")
    parser_align.set_defaults(func=run_gui_align)

    parser_main = subparsers.add_parser('main', help='Run main GUI')
    parser_main.set_defaults(func=run_gui_classical)
    parser_main.add_argument("files", nargs="*", help="File(s) to run")

    parser_main = subparsers.add_parser('legacy', help='Run Legacy Qt GUI')
    parser_main.set_defaults(func=run_gui_legacy)
    parser_main.add_argument("files", nargs="*", help="File(s) to run")

    parser_version = subparsers.add_parser('version', help='Get version information')
    parser_version.set_defaults(func=print_version)

    for subparser in subparsers.choices.values():
        subparser.add_argument("--debug", help="Sets logging level to debug", action="store_true")
    return parser


def get_args(args=None):
    """
    Create a parser and return the parsed command line arguments
    """
    parser = create_parser()
    args = parser.parse_args(args=args)
    return args


def run_cli(argv=None):
    """
    Parse the command line arguments and
    start the appropriate function
    """
    parser = create_parser()
    argsin = parser.parse_args(args=argv)
    if argsin.version:
        print_version()
        return
    args = vars(argsin)
    debug = args.pop("debug", False)
    args.pop("version")
    if debug:
        set_loggers_to_debug()
        utility.set_loggers_to_debug()
        logger.info("Logging level set to debug")
    func = args.pop("func", None)
    if func is None:
        logger.error("Run mode required unless querying version")
        parser.print_help()
        return

    func(**args)

def print_version():
    print(version.__version__)
