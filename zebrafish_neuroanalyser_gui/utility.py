import logging

def set_loggers_to_level(level, verbose=False):
    """ Set all handlers to level"""
    root_logger = logging.getLogger(__package__)
    for handler in root_logger.handlers:
        handler.setLevel(level)
    root_logger.setLevel(level)
    if verbose:
        logging.getLogger(__name__).info("Logging level set to %s", logging.getLevelName(level))

def set_loggers_to_debug():
    """ Set all handlers to debug"""
    set_loggers_to_level(logging.DEBUG)
