
# -*- coding: utf-8 -*-
"""Test the command line interface functions

J. Metz <metz.jp@gmail.com>
"""
import unittest
from unittest import mock
import argparse
# import logging
from zebrafish_neuroanalyser import cli


class TestCli(unittest.TestCase):
    """Main cli test case"""

    @mock.patch('zebrafish_neuroanalyser.cli.run_gui_classical')
    def test_run_cli(self, mock_run_gui_classical):  # pylint: disable=no-self-use
        """Test with no inputs - should present a basic gui"""
        cli.run_cli(argv=[])
        mock_run_gui_classical.assert_called_once()

    @mock.patch('zebrafish_neuroanalyser.main.main')
    def test_run_cli__cli(self, mock_main):  # pylint: disable=no-self-use
        """Test that run_cli works in cli mode, albeit without inputs"""
        with self.assertRaises(ValueError):
            cli.run_cli(argv=["--cli"])
        # mock_main.assert_called_once()

    @mock.patch('zebrafish_neuroanalyser.main.main')
    def test_run_cli_cli_with_failure(self, mainfunc):
        """Test that failure is passed down"""
        mainfunc.side_effect = Exception("MAIN FAIL")
        with self.assertRaises(Exception):
            cli.run_cli(argv=["--cli"])

    @mock.patch('os.symlink')
    @mock.patch('zebrafish_neuroanalyser.cli.run_gui_align')
    def test_run_cli_align_gui(self, mock_run, _):  # pylint: disable=no-self-use
        """Test that run_gui_align gets run"""
        cli.run_cli(argv=["--align"])
        mock_run.assert_called_once()


class TestCreateParser(unittest.TestCase):
    """Test the parser creating funcionality in cli"""
    def test_create_parser(self):
        """Test that a parser is created"""
        res = cli.create_parser()
        self.assertIsInstance(res, argparse.ArgumentParser)
