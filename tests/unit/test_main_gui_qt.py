#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test GUI functions
#

import unittest
from unittest import mock
import zebrafish_neuroanalyser.gui.qt5 as gui

app = gui.QtWidgets.QApplication([])

class Test_main_gui(unittest.TestCase):
    def test_create_main_gui(self):
        # Check that we create the correct object
        dialog = gui.MainDialog(lambda f: None)
        self.assertTrue(isinstance(dialog, gui.MainDialog))
